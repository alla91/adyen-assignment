module.exports = {
    root: true,
    env: {
        browser: true,
        'jest/globals': true
    },
    'extends': [
        'plugin:vue/essential',
        '@vue/standard'
    ],
    rules: {
        'generator-star-spacing': 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
    },
    parserOptions: {
        parser: 'babel-eslint'
    },
    plugins: [
        'vue', 'jest'
    ]
}