# Running the project

1. `cd` to the **/adyen-assginment** directory
2. `yarn` or `npm install` for downloading node_modules
3. `yarn serve` or `npm run serve`

# Running the tests
1. `cd` to the **/adyen-assginment** directory
2. `yarn test:unit` or `npm run test:unit`

# Environment variables
If you need to run the project locally, create .env.local file with the following content
VUE_APP_CLIENT_ID=OBEWQDFOYDLFYDGXVPWPBBH40WA4YU0LTJRWXONPE55NAMEN
VUE_APP_CLIENT_SECRET=`will be provided by email`

# Enjoy!