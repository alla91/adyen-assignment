const venueTypes = [
  {
    name: 'Food',
    picture: 'https://i.ytimg.com/vi/NCO36DCleZ8/hqdefault.jpg',
    type: 'food'
  },
  {
    name: 'Drinks',
    picture: 'https://3c1703fe8d.site.internapcdn.net/newman/gfx/news/hires/2017/ismixingdrin.jpg',
    type: 'drinks'
  },
  {
    name: 'Coffee',
    picture: 'https://financialtribune.com/sites/default/files/field/image/17january/04-ff-coffee_120-ab.jpg',
    type: 'coffee'
  },
  {
    name: 'Arts',
    picture: 'http://assets.londonist.com/uploads/2016/01/street_art.jpg',
    type: 'arts'
  },
  {
    name: 'Outdoors',
    picture: 'https://d5qsyj6vaeh11.cloudfront.net/images/whats%20available/editorial%20orphan%20articles/article%20images/w2206_b_main.jpg',
    type: 'outdoors'
  },
  {
    name: 'Top picks',
    picture: 'https://c1.staticflickr.com/3/2292/2478311790_6941ce2c88_z.jpg?zz=1',
    type: 'topPicks'
  }
]

export default venueTypes
