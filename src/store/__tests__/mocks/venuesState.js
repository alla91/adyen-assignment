const state = {
  list: [
    {
      id: '1',
      name: 'test name 1',
      location: 'test location 1',
      details: 'test details 1'
    },
    {
      id: '2',
      name: 'test name 2',
      location: 'test location 2',
      details: 'test details 2'
    },
    {
      id: '3',
      name: 'test name 3',
      location: 'test location 3',
      details: 'test details 3'
    }
  ],
  venueType: 'coffee',
  location: {
    latitude: 52.3702,
    longitude: 4.8952
  }
}

export default state