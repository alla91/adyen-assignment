import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import venues from '../../modules/venues'
import venuesStateMock from '../mocks/venuesState'
import * as types from '../../mutation-types'

describe('Venues', () => {
  let API = new MockAdapter(axios)

  describe('State', () => {
    it('should contain the same keys as the mock', () => {
      let keysInModule = Object.keys(venues.state)
      let keysInMock = Object.keys(venuesStateMock)
      expect(keysInModule).toEqual(keysInMock)
    })
  })

  describe('Getters', () => {
    let state
    beforeEach(() => {
      state = {...venuesStateMock}
    })

    it('returns the venues list on getVenuesList', () => {
      const getterResult = venues.getters.getVenuesList(state)
      expect(getterResult).toEqual(venuesStateMock.list)
    })
  })

  describe('Actions', () => {
    const hostname = 'https://api.foursquare.com/v2'
    let commit = {
      count: 0,
      payload: ''
    }

    beforeEach(() => {
      commit.count = 0
      commit.payload = ''
    })

    afterEach(() => {
      API.reset()
    })

    describe('getVenues', () => {
      let path = `${hostname}/venues/explore`
      let mockResponse = {
        response: {
          groups: [
            {
              items: []
            }
          ]
        }
      }
      describe('onSuccess', () => {
        let mockCommit = (type) => {
          if (type === types.RECEIVE_VENUES) {
            commit.count += 1
          }
        }

        it('it commits RECEIVE_VENUES', async () => {
          API.onGet(path).reply(200, mockResponse)
          await venues.actions.getVenues({commit: mockCommit, state: venues.state}, { radius: 500, priceRange: '1,2' })
          expect(commit.count).toEqual(1)
        })
      })

      describe('onError', () => {
        let mockCommit = () => {
          commit.count += 1
        }

        it('does not commit anything', (done) => {
          API.onGet(path).reply(404, 'Not found')
          venues.actions.getVenues({commit: mockCommit, state: venues.state}, { radius: 500, priceRange: '1,2' })
              .catch(() => {
                expect(commit.count).toEqual(0)
                done()
              })
        })
      })
    })
  })

  describe('Mutations', () => {
    it('RECEIVE_VENUES mutation sets the venues list correctly', () => {
      venues.mutations[types.RECEIVE_VENUES](venues.state, {venues: venuesStateMock.list})
      expect(venues.state.list).toEqual(venuesStateMock.list)
    })

    it('SET_VENUE_TYPE mutation sets the venue type correctly', () => {
      venues.mutations[types.SET_VENUE_TYPE](venues.state, {type: 'food'})
      expect(venues.state.venueType).toEqual('food')
    })

    it('SET_LOCATION mutation sets the location correctly', () => {
      venues.mutations[types.SET_LOCATION](venues.state, {latitude: 1, longitude: 2})
      expect(venues.state.location.latitude).toEqual(1)
      expect(venues.state.location.longitude).toEqual(2)
    })
  })
})