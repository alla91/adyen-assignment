import Vue from 'vue'
import Vuex from 'vuex'
import venues from './modules/venues'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    venues
  }
})
