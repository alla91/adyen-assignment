import api from '../../api'
import * as types from '../mutation-types'

const namespaced = true

const state = {
  list: [],
  venueType: '',
  location: {
    latitude: 52.3702,
    longitude: 4.8952
  }
}

const getters = {
  getVenuesList: state => state.list
}

const actions = {
  async getVenues ({commit, state}, {radius, priceRange}) {
    await api.getVenues(
      state.location,
      state.venueType,
      radius,
      priceRange,
      (response) => {
        let venues = response.groups[0].items.map(item => {
          return {
            id: item.venue.id,
            name: item.venue.name,
            location: item.venue.location.address,
            details: item.venue.categories[0].name
          }
        })
        commit(types.RECEIVE_VENUES, {venues: venues})
        return Promise.resolve()
      },
      (error) => {
        return Promise.reject(error)
      }
    )
  }
}

const mutations = {
  [types.RECEIVE_VENUES] (state, {venues}) {
    Object.assign(state.list, venues)
  },
  [types.SET_VENUE_TYPE] (state, {type}) {
    state.venueType = type
  },
  [types.SET_LOCATION] (state, {latitude, longitude}) {
    state.location = {
      latitude: latitude,
      longitude: longitude
    }
  }
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
