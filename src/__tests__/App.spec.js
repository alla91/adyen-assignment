import { shallowMount, createLocalVue } from '@vue/test-utils'
import App from '../App'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('App', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    store = new Vuex.Store({})
    wrapper = shallowMount(App, { store, localVue })
  })

  describe('When App is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Methods', () => {
    it('goes to the correct step', () => {
      wrapper.vm.goToStep(3)
      expect(wrapper.vm.step).toEqual(3)
    })

    it('shows results correctly', () => {
      wrapper.vm.$store.dispatch = jest.fn()
      wrapper.vm.showResults({radius: 200, priceRange: '1,2,3'})
      expect(wrapper.vm.$store.dispatch).toHaveBeenCalledWith('venues/getVenues', { radius: 200, priceRange: '1,2,3' })
    })

    it('sets location correctly', () => {
      wrapper.vm.$store.commit = jest.fn()
      wrapper.vm.setLocation({
        coords: {
          latitude: 1,
          longitude: 2
        }
      })
      expect(wrapper.vm.$store.commit).toHaveBeenCalledWith('venues/SET_LOCATION', { latitude: 1, longitude: 2 })
    })
  })
})
