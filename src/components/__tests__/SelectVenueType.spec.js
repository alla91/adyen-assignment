import { shallowMount, createLocalVue } from '@vue/test-utils'
import SelectVenueType from '../SelectVenueType'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('SelectVenueType', () => {
  let localVue
  let wrapper
  let store

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    store = new Vuex.Store({})
    wrapper = shallowMount(SelectVenueType, { store, localVue })
  })

  describe('When SelectVenueType is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Methods', () => {
    it('selects the venue type correctly', () => {
      wrapper.vm.$store.commit = jest.fn()
      wrapper.vm.$emit = jest.fn()
      wrapper.vm.selectVenueType('coffee')
      expect(wrapper.vm.$store.commit).toHaveBeenCalledWith('venues/SET_VENUE_TYPE', {type: 'coffee'})
      expect(wrapper.vm.$emit).toHaveBeenCalledWith('goToStep', 2)
    })
  })
})