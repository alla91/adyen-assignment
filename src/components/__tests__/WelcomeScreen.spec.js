import { shallowMount, createLocalVue } from '@vue/test-utils'
import WelcomeScreen from '../WelcomeScreen'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('WelcomeScreen', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    wrapper = shallowMount(WelcomeScreen, { localVue })
  })

  describe('When WelcomeScreen is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})