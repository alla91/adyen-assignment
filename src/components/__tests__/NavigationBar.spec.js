import { shallowMount, createLocalVue } from '@vue/test-utils'
import NavigationBar from '../NavigationBar'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('NotBored', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    wrapper = shallowMount(NavigationBar, {
      localVue,
      propsData: {
        step: 1
      }
    })
  })

  describe('When NavigationBar is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Methods', () => {
    it('navigates to the correct step', () => {
      wrapper.vm.$emit = jest.fn()
      wrapper.setProps({step: 2})
      wrapper.vm.goBack()
      expect(wrapper.vm.$emit).toHaveBeenCalledWith('goToStep', 1)
      wrapper.setProps({step: -1})
      wrapper.vm.goBack()
      expect(wrapper.vm.$emit).toHaveBeenCalledWith('goToStep', 0)
    })
  })
})