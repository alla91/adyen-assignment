import { shallowMount, createLocalVue } from '@vue/test-utils'
import ViewResults from '../ViewResults'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('ViewResults', () => {
  let localVue
  let wrapper
  let store

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
  })

  describe('When ViewResults is mounted', () => {
    it('has the expected html structure if no results are provided', () => {
      store = new Vuex.Store({
        getters: {
          'venues/getVenuesList': () => []
        }
      })
      wrapper = shallowMount(ViewResults, { store, localVue })
      expect(wrapper.element).toMatchSnapshot()
    })

    it('has the expected html structure if results are provided', () => {
      store = new Vuex.Store({
        getters: {
          'venues/getVenuesList': () => [{
            id: 1,
            name: 'test name',
            details: 'test details',
            location: 'test location'
          }]
        }
      })
      wrapper = shallowMount(ViewResults, { store, localVue })
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})