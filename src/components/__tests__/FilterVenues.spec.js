import { shallowMount, createLocalVue } from '@vue/test-utils'
import FilterVenues from '../FilterVenues'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('FilterVenues', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    wrapper = shallowMount(FilterVenues, { localVue })
  })

  describe('When FilterVenues is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Methods', () => {
    it('shows results correctly', () => {
      wrapper.vm.priceRange = [1, 2]
      wrapper.vm.distanceRadius = 200
      wrapper.vm.$emit = jest.fn()
      wrapper.vm.showResults()
      expect(wrapper.vm.$emit).toHaveBeenCalledWith('showResults', { radius: 200, priceRange: '1,2' })
    })
  })
})