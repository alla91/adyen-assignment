import { shallowMount, createLocalVue } from '@vue/test-utils'
import VenueTypeCard from '../VenueTypeCard'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('VenueTypeCard', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    wrapper = shallowMount(VenueTypeCard, {
      localVue,
      propsData: {
        name: 'TestName',
        type: 'testType',
        picture: 'testUrl'
      }
    })
  })

  describe('When VenueTypeCard is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})