import { shallowMount, createLocalVue } from '@vue/test-utils'
import NotBored from '../NotBored'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('NotBored', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    wrapper = shallowMount(NotBored, { localVue })
  })

  describe('When NotBored is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})