import axios from 'axios'

const hostname = 'https://api.foursquare.com/v2'

const api = {
  getVenues: async (location, venueType, radius, priceRange, onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'GET',
        url: `${hostname}/venues/explore`,
        params: {
          client_id: process.env.VUE_APP_CLIENT_ID,
          client_secret: process.env.VUE_APP_CLIENT_SECRET,
          ll: `${location.latitude},${location.longitude}`,
          section: venueType,
          v: '20180807',
          limit: 100,
          radius: radius,
          price: priceRange
        }
      })
      return onSuccess(response.data.response)
    } catch (error) {
      return onError(error)
    }
  }
}

export default api
